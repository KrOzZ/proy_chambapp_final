package com.tecktools.proy_chambapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tecktools.proy_chambapp.R;
import com.tecktools.proy_chambapp.clases.Trabajos;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class TrabajoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{


    private Context context;
    private int textViewResourceID;
    private ArrayList<Trabajos> items;
    private OnItemClickListener mOnItemClickListener;

    public TrabajoAdapter(ArrayList<Trabajos> items, Context context) {
        this.items = items;
        this.context = context;
    }

    public void setOnItemClickListener(OnItemClickListener param){
        this.mOnItemClickListener = param;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OriginalViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_item_trabajo, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        OriginalViewHolder holder1 = (OriginalViewHolder) holder;
        Trabajos trabajos = items.get(position);

        holder1.lbTitulo.setText(trabajos.getTrabajo());
        //holder1.lbDescricpion.setText(trabajos.getTipo());
        switch (trabajos.getTipo()){
            case 1:
                holder1.lbDescricpion.setText("Informatica");
                holder1.imagenPerfil.setImageResource(R.drawable.image_30);
                break;
            case 2:
                holder1.lbDescricpion.setText("Hoteleria");
                holder1.imagenPerfil.setImageResource(R.drawable.image_26);
                break;
            case 3:
                holder1.lbDescricpion.setText("Comercio");
                holder1.imagenPerfil.setImageResource(R.drawable.image_18);
                break;
            case 4:
                holder1.lbDescricpion.setText("Restaurante");
                holder1.imagenPerfil.setImageResource(R.drawable.image_24);
                break;
            case 5:
                holder1.lbDescricpion.setText("Medicina");
                holder1.imagenPerfil.setImageResource(R.drawable.image_20);
                break;
            case 6:
                holder1.lbDescricpion.setText("Ingeneria");
                holder1.imagenPerfil.setImageResource(R.drawable.image_2);
                break;
            case 7:
                holder1.lbDescricpion.setText("Servicios");
                holder1.imagenPerfil.setImageResource(R.drawable.image_19);
                break;
        }
        holder1.lbSueldo.setText(formatoDinero(trabajos.getSueldo()));
        holder1.lytParent.setOnClickListener(v-> mOnItemClickListener.onItemClick(v, items.get(position), position));

    }
    public interface OnItemClickListener {
        void onItemClick(View param1View, Trabajos param1People, int param1Int);
    }

    public String formatoDinero(float dinero){
        return NumberFormat.getCurrencyInstance(new Locale("es","MX")).format(dinero);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class OriginalViewHolder extends RecyclerView.ViewHolder{

        LinearLayout lytParent;
        ImageView imagenPerfil;
        TextView lbTitulo;
        TextView lbDescricpion;
        TextView lbSueldo;


        public OriginalViewHolder(@NonNull View itemView) {
            super(itemView);
            lytParent = itemView.findViewById(R.id.lyt_parent);
            imagenPerfil = itemView.findViewById(R.id.imgSelect);
            lbTitulo = itemView.findViewById(R.id.lbTitulo);
            lbDescricpion = itemView.findViewById(R.id.lbCategoria);
            lbSueldo = itemView.findViewById(R.id.lbPrecio);

        }
    }
}

