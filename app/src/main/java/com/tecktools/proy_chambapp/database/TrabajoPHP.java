package com.tecktools.proy_chambapp.database;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tecktools.proy_chambapp.clases.Trabajos;

import java.util.HashMap;
import java.util.Map;

public class TrabajoPHP {
    private RequestQueue request;
    private Context context;

    public TrabajoPHP(Context context){
        this.context = context;
        this.request = Volley.newRequestQueue(context);
    }

    public void insertTrabajo(Trabajos trabajo, Response.Listener<String> yes, Response.ErrorListener error){
        StringRequest requestString = new StringRequest(Request.Method.POST, BaseURL.URL_AGREGAR_TRABAJO, yes, error){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                try {
                    params.put("trabajo", new ObjectMapper().writeValueAsString(trabajo));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                return params;

            }
        };
        request.add(requestString);


    }

    public void consultarTrabajos(Response.Listener<String> yes, Response.ErrorListener error){
        StringRequest requestString = new StringRequest(Request.Method.POST, BaseURL.URL_CONSULTAR_TODOS, yes, error);
        request.add(requestString);
    }
}
