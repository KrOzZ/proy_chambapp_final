package com.tecktools.proy_chambapp.database;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tecktools.proy_chambapp.clases.Usuarios;

import java.util.HashMap;
import java.util.Map;

public class UserPHP {
    private RequestQueue request;
    private Context context;

    public UserPHP(Context context){
        this.context = context;
        this.request = Volley.newRequestQueue(context);
    }

    public void insertUser(Usuarios usuarios, Response.Listener<String> yes, Response.ErrorListener error){
        StringRequest requestString = new StringRequest(Request.Method.POST, BaseURL.URL_AGREGAR_Usuario, yes, error){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                try {
                    params.put("user", new ObjectMapper().writeValueAsString(usuarios));
                    Log.e("D", new ObjectMapper().writeValueAsString(usuarios));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                return params;

            }
        };
        request.add(requestString);


    }
}
