package com.tecktools.proy_chambapp.database;

public class BaseURL {

    //private static final String baseURl = "http://192.168.0.11/ChambApp_DB/";
    private static final String baseURl = "https://vivamazatlantours.com/Practicas/ChambApp_DB/";

    public static final String URL_REGISTRAR = baseURl + "wsRegistro.php";
    public static final String URL_ACUTALIZAR = baseURl + "wsActualizar.php";
    public static final String URL_CONSULTAR_TODOS = baseURl + "Funciones.php?op=consult";
    public static final String URL_CONSULTAR_TODOSMios = baseURl + "Funciones.php?op=consultTrabajoId";
    public static final String URL_ELIMINAR = baseURl + "wsEliminar.php";
    public static final String URL_AGREGAR_TRABAJO = baseURl + "Funciones.php?op=insert";
    public static final String subirImagen = baseURl + "Funciones.php?op=SubirImagen";
    public static final String URL_AGREGAR_Usuario = baseURl + "Funciones.php?op=insertUsuario";
}
