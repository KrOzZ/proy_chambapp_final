package com.tecktools.proy_chambapp.utils;

import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.recyclerview.widget.RecyclerView;

public abstract class LoaderList {

    public static void showLoading(LinearLayout lyt_progress, RecyclerView list)
    {
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0F);
        list.setVisibility(View.GONE);
    }

    public static void showLoading(LinearLayout lyt_progress, RecyclerView list, LinearLayout lyt_empty)
    {
        (new Handler()).postDelayed(() -> ViewAnimation.fadeOut(lyt_empty),  200L);
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0F);

        list.setVisibility(View.GONE);
    }
    public static void showLoading(LinearLayout lyt_progress, ListView list, LinearLayout lyt_empty)
    {
        (new Handler()).postDelayed(() -> ViewAnimation.fadeOut(lyt_empty),  200L);
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0F);

        list.setVisibility(View.GONE);
    }

    public static void showList(LinearLayout lyt_progress, RecyclerView list)
    {
        (new Handler()).postDelayed(() -> ViewAnimation.fadeOut(lyt_progress),  800L);
        (new Handler()).postDelayed(() -> list.setVisibility(View.VISIBLE), 1000L);
    }

    public static void showLoading(LinearLayout lyt_progress, ListView list)
    {
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0F);
        list.setVisibility(View.GONE);
    }

    public static void showList(LinearLayout lyt_progress, ListView list)
    {
        (new Handler()).postDelayed(() -> ViewAnimation.fadeOut(lyt_progress),  800L);
        (new Handler()).postDelayed(() -> list.setVisibility(View.VISIBLE), 1000L);
    }

    public static void showEmpty(LinearLayout lyt_progress, LinearLayout lyt_empty)
    {
        (new Handler()).postDelayed(() -> ViewAnimation.fadeOut(lyt_progress),  800L);
        (new Handler()).postDelayed(() -> lyt_empty.setVisibility(View.VISIBLE), 1000L);
    }

}

