package com.tecktools.proy_chambapp.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Build;

import androidx.core.content.FileProvider;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.tecktools.proy_chambapp.clases.Documento;
import com.tecktools.proy_chambapp.clases.TipoDoc;
import com.tecktools.proy_chambapp.clases.Trabajos;
import com.tecktools.proy_chambapp.database.BaseURL;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class GuardarImagen {

    private ProgressDialog p;
    private String documentPath;
    private Context context;
    private RequestQueue mRequestQueue;
    private Trabajos trabajos;
    private TipoDoc doc;
    private Documento documento = null;
    private boolean isUpdate = false;


    public GuardarImagen(String documentPath, Context context, Trabajos trabajos, TipoDoc doc) {
        this.documentPath = documentPath;
        this.context = context;
        this.mRequestQueue = Volley.newRequestQueue(this.context);
        this.trabajos = trabajos;
        this.doc = doc;
        showDialogLoad();

    }
    public GuardarImagen(String documentPath, Context context, Trabajos trabajos, Documento docs, boolean isUpdate) {
        this.documentPath = documentPath;
        this.context = context;
        this.mRequestQueue = Volley.newRequestQueue(this.context);
        this.trabajos = trabajos;
        this.documento = docs;
        this.isUpdate = isUpdate;
        showDialogLoad();

    }


    private void showDialogLoad() {
        p = new ProgressDialog(context);
        p.setTitle("Preparando documeto");
        p.setMessage("Por favor espere...");
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.show();
    }

    public void closeDialog() {
        p.dismiss();
    }

    private Uri getUriDocument()
    {
        Uri uri = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            File file = new File(documentPath);
            uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
        }
        else
        {
            uri = Uri.parse(documentPath);
        }
        return uri;
    }

    private String getNameFile()
    {
        String format = "%s_%s.pdf";
        if(documento == null)
            format = "%s.pdf";
        return String.format(format, Hashing.md5().hashString(trabajos.getTitulo(), Charsets.UTF_8));

    }

    public void uploadDoc(Response.Listener<NetworkResponse> success, Response.ErrorListener error) {

        p.setTitle("Subiendo documento");
        try {
            InputStream input = context.getContentResolver().openInputStream(getUriDocument());
            final byte[] inputData = getBytes(input);
            VolleyMultipartRequest request = new
                    VolleyMultipartRequest(Request.Method.POST, BaseURL.subirImagen, success, error) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            params.put("idalumno", String.valueOf(trabajos.getId()));
                            // params.put("tags", "ccccc");  add string parameters
                            return params;
                        }

                        @Override
                        protected Map<String, DataPart> getByteData() {
                            Map<String, DataPart> params = new HashMap<>();
                            params.put("doc", new DataPart(getNameFile(), inputData));
                            return params;
                        }
                    };
            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequestQueue.add(request);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }


}
