package com.tecktools.proy_chambapp.clases;

public class TipoDoc {
    private int id;
    private String doc;

    public TipoDoc(int id, String doc) {
        this.id = id;
        this.doc = doc;
    }

    public TipoDoc()
    {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String toString()
    {
        return this.doc;
    }
}
