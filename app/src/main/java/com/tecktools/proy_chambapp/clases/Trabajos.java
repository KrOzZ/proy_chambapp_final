package com.tecktools.proy_chambapp.clases;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Trabajos implements Serializable {
    private int id;
    private String trabajo;
    private String telefono;
    private String direccion;
    private String empresa;
    private String email;
    private String titulo;
    private String descripcion;
    private String web;
    private float sueldo;
    private int tipo;
    //private String urlImageFoto;
    //private String secureId;
    private int status;

    public Trabajos()
    {

    }



    public Trabajos(JSONObject object)
    {
        try {
            this.setId(object.getInt("_ID"));
            this.setTrabajo(object.getString("trabajo"));
            this.setTelefono((object.getString("telefono")));
            this.setDireccion(object.getString("direccion"));
            this.setEmail(object.getString("email"));
            this.setEmpresa(object.getString("empresa"));
            this.setTitulo(object.getString("titulo"));
            this.setDescripcion(object.getString("descripcion"));
            this.setWeb(object.getString("web"));
            this.setSueldo(object.getLong("sueldo"));
            this.setTipo(object.getInt("tipo"));
            //this.setUrlImageFoto(object.getString("foto"));
            //this.setSecureId(object.getString("idMovil"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public Trabajos(int id, String trabajo, String telefono, String direccion, String email, String empresa, String titulo, String descripcion, String web, float sueldo, int tipo,/*String urlImageFoto, String secureId,*/ int status) {
        this.id = id;
        this.trabajo = trabajo;
        this.telefono = telefono;
        this.direccion = direccion;
        this.empresa = empresa;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.email= email;
        this.web = web;
        this.sueldo = sueldo;
        this.tipo = tipo;
        //this.urlImageFoto = urlImageFoto;
        //this.secureId = secureId;
        //this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public float getSueldo() {
        return sueldo;
    }

    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    /*public String getUrlImageFoto() {
        return urlImageFoto;
    }

    public void setUrlImageFoto(String urlImageFoto) {
        this.urlImageFoto = urlImageFoto;
    }
*/

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
