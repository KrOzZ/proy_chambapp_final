package com.tecktools.proy_chambapp.clases;

import org.json.JSONException;
import org.json.JSONObject;

public class Usuarios {
    private int id;
    private String user;
    private String password;
    private String nombre;
    private String email;
    private String tel;
    private String direc;
    private String face;
    private boolean tipo;
    private String secureId;
    private int status;

    public Usuarios()
    {

    }



    public Usuarios(JSONObject object)
    {
        try {
            this.setId(object.getInt("_ID"));
            this.setNombre(object.getString("nombre"));
            this.setUser(object.getString("user"));
            this.setPassword(object.getString("password"));
            this.setEmail(object.getString("email"));
            //this.setTel(object.getString("telefono"));
            //this.setDirec(object.getString("direccion"));
            //this.setFace(object.getString("facebook"));
            this.setTipo(object.getInt("tipo") == 1);
            //this.setSecureId(object.getString("idMovil"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public Usuarios(int id, String user, String password, String nombre, String email, String tel, String direc, String face, boolean tipo, String secureId, int status) {
        this.id = id;
        this.user = user;
        this.password = password;
        this.nombre = nombre;
        this.email = email;
        this.tel = tel;
        this.direc = direc;
        this.face = face;
        this.tipo = tipo;
        this.secureId = secureId;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDirec() {
        return direc;
    }

    public void setDirec(String direc) {
        this.direc = direc;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public boolean isTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }

    public String getSecureId() {
        return secureId;
    }

    public void setSecureId(String secureId) {
        this.secureId = secureId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
