package com.tecktools.proy_chambapp.activitys;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;
import com.tecktools.proy_chambapp.R;
import com.tecktools.proy_chambapp.clases.Usuarios;
import com.tecktools.proy_chambapp.database.Config;
import com.tecktools.proy_chambapp.database.UserPHP;

public class RegisCuentaActivity extends AppCompatActivity {

    private ImageButton imageButton5;
    private TextInputEditText txtUser;
    private ImageButton imageButton2;
    private TextInputEditText txtPass;
    private ImageButton imageButton3;
    private TextInputEditText txtPass2;
    private ImageButton imageButton4;
    private TextInputEditText txtNombre;
    private ImageButton imageButton1;
    private TextInputEditText txtEmail;
    private RadioGroup genero;
    private MaterialRadioButton radioFemale;
    private MaterialRadioButton radioMale;
    private CheckBox cbTipo;
    private LinearLayout signUpForAccount;
    private TextView btnRegistro;
    private UserPHP php;
    private long ID;
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis_cuenta);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        php = new UserPHP(this);
        imageButton5 = findViewById(R.id.imageButton5);
        txtUser = findViewById(R.id.txtUser);
        imageButton2 = findViewById(R.id.imageButton2);
        txtPass = findViewById(R.id.txtPass);
        imageButton3 = findViewById(R.id.imageButton3);
        txtPass2 = findViewById(R.id.txtPass2);
        imageButton4 = findViewById(R.id.imageButton4);
        txtNombre = findViewById(R.id.txtNombre);
        imageButton1 = findViewById(R.id.imageButton1);
        txtEmail = findViewById(R.id.txtEmail);
        genero = findViewById(R.id.genero);
        radioFemale = findViewById(R.id.radio_female);
        radioMale = findViewById(R.id.radio_male);
        cbTipo = findViewById(R.id.cbTipo);
        signUpForAccount = findViewById(R.id.sign_up_for_account);
        btnRegistro = findViewById(R.id.btnRegistro);
        btnRegistro.setOnClickListener(this::btnGuardarAction);
    }


    private void btnGuardarAction(View view) {
        if(!validar()) {
            Toast.makeText(getApplicationContext(), "No puedes dejar los campos vacios",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if(!Config.isInternetConnection(getApplicationContext()))
        {
            Toast.makeText(getApplicationContext(), "Requiere conexion a internet",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Usuarios usuarios = new Usuarios();
        usuarios.setNombre(getText(txtNombre));
        usuarios.setUser(getText(txtUser));
        usuarios.setEmail(getText(txtEmail));
        usuarios.setPassword(getText(txtPass));
        usuarios.setTipo(cbTipo.isChecked());
        //contacto.setSecureId(Config.getSecureId(getApplicationContext()));
        if (isEdit)
        {
            usuarios.setId( (int) ID);
            //ws.updateContacto(contacto);
            Toast.makeText(getApplicationContext(), "Se actualizo correctamente", Toast.LENGTH_SHORT).show();
        }
        else{
            php.insertUser(usuarios, this::registerSucces, error -> {
                Log.e("Error", error.getMessage());
            });
        }
        limpiar();

    }
    private void registerSucces(String res){
        Log.e("DATA", res);
        Toast.makeText(getApplicationContext(), "Se agrego correctamente", Toast.LENGTH_SHORT).show();

    }
    private void limpiar(){
        txtNombre.setText("");
        txtEmail.setText("");
        txtPass.setText("");
        txtPass2.setText("");
        txtUser.setText("");
        cbTipo.setChecked(false);
        isEdit = false;
        ID = 0;
    }


    private boolean validar(){
        if(validarCampo(txtEmail) || validarCampo(txtNombre) ||
                validarCampo(txtPass) || validarCampo(txtPass2) || validarCampo(txtUser))
            return false;
        else
            return true;
    }
    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }
}
