package com.tecktools.proy_chambapp.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import com.android.volley.RequestQueue;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialAutoCompleteTextView;
import com.tecktools.proy_chambapp.R;
import com.tecktools.proy_chambapp.clases.TipoDoc;
import com.tecktools.proy_chambapp.clases.Trabajos;
import com.tecktools.proy_chambapp.database.Config;
import com.tecktools.proy_chambapp.database.TrabajoPHP;
import com.tecktools.proy_chambapp.utils.AlertDialogCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisTrabajoActivity extends AppCompatActivity {

    private NestedScrollView nestedScrollView;
    private LinearLayout lyt3Form;
    private LinearLayout lyt2Form;
    private LinearLayout lytForm;
    private TextInputEditText txtTrabajo;
    private TextInputEditText txtTelefono;
    private TextInputEditText txtEmail;
    private ImageButton imageButton2;
    private TextInputEditText txtDireccion;
    private TextInputEditText txtEmpresa;
    //private TextInputEditText txtTitulo;
    private TextInputEditText txtWeb;
    private TextInputEditText txtSueldo;
    private TextInputEditText txtTipo;
    private Spinner spnTipo;
    private FloatingActionsMenu gpFab;
    private FloatingActionButton fabAgregar;
    private FloatingActionButton fabCamara;
    private ListTrabajoActivity listTrabajo;
    private MaterialAutoCompleteTextView txtDescripcion;
    //
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private String currentPhotoPath;
    private TipoDoc SELECTED_DOCUMENT;
    private Trabajos trabajos;
    private RequestQueue queue;
    //
    private long ID;
    private long tipo;
    private boolean isEdit = false;
    private TrabajoPHP php;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis_trabajo);

        php = new TrabajoPHP(this);
        nestedScrollView = findViewById(R.id.nested_scroll_view);
        lyt3Form = findViewById(R.id.lyt3_form);
        lyt2Form = findViewById(R.id.lyt2_form);
        lytForm = findViewById(R.id.lyt_form);
        txtTrabajo = findViewById(R.id.txtTrabajo);
        txtTelefono = findViewById(R.id.txtTelefono);
        txtEmail = findViewById(R.id.txtEmail);
        imageButton2 = findViewById(R.id.imageButton2);
        txtDireccion = findViewById(R.id.txtDireccion);
        txtEmpresa = findViewById(R.id.txtEmpresa);
        //txtTitulo = findViewById(R.id.txtTitulo);
        txtWeb = findViewById(R.id.txtWeb);
        spnTipo = findViewById(R.id.spTipo);
        txtSueldo = findViewById(R.id.txtSueldo);
        gpFab = findViewById(R.id.gpFab);
        fabAgregar = findViewById(R.id.fabAgregar);
        fabCamara = findViewById(R.id.fabCamara);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        unit();
        //Bundle datos = getIntent().getExtras();
        //lblUser.setText("Nombre: " + datos.getString("user"));
        //this.listTrabajo = (ListTrabajoActivity) getIntent().getExtras().getSerializable("trabajo");
    }
    private void unit()
    {
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(RegisTrabajoActivity.this,android.R.layout.simple_expandable_list_item_1, getResources().getStringArray(R.array.tipoTrabajoArray));
        spnTipo.setAdapter(adapter);
        spnTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String pais= parent.getItemAtPosition(position).toString();
                tipo = parent.getItemIdAtPosition(position);
                //Toast.makeText(RegisTrabajoActivity.this,tipo+":"+ pais ,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        FloatingActionButton fabAgregar= findViewById(R.id.fabAgregar);
        FloatingActionButton fabCamara= findViewById(R.id.fabCamara);
        //mImagenPerfil = findViewById(R.id.imagenPerfil);
        fabAgregar.setOnClickListener(this::functionAgregar);
        //Picasso.get().load(trabajos.getUrlImageFoto()).into(mImagenPerfil);
    }


    private void functionAgregar(View view){
        Toast.makeText(this, "Agregado con exito", Toast.LENGTH_SHORT).show();
        if(!validar()) {
            Toast.makeText(getApplicationContext(), "No puedes dejar los campos vacios",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if(!Config.isInternetConnection(getApplicationContext()))
        {
            Toast.makeText(getApplicationContext(), "Requiere conexion a internet",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Trabajos trabajos = new Trabajos();
        trabajos.setTrabajo(getText(txtTrabajo));
        trabajos.setTelefono(getText(txtTelefono));
        trabajos.setDireccion(getText(txtDireccion));
        trabajos.setEmail(getText(txtEmail));
        trabajos.setEmpresa(getText(txtEmpresa));
        //trabajos.setTitulo(getText(txtTitulo));
        trabajos.setDescripcion(getText(txtDescripcion));
        trabajos.setWeb(getText(txtWeb));
        trabajos.setTipo((int) tipo);
        //trabajos.setTipo(Integer.parseInt(txtTipo.getText().toString()));
        trabajos.setSueldo(Float.parseFloat(txtSueldo.getText().toString()));

        if (isEdit)
        {
            trabajos.setId( (int) ID);
            //ws.updateContacto(contacto);

        }
        else{
            php.insertTrabajo(trabajos, this::registerSucces, error -> {
                Log.e("Error", error.getMessage());
            });

        }
        //limpiar();
    }



    private void registerSucces(String res){

        try {
            JSONObject json = new JSONObject(res);
            if(json.getBoolean("status")){
                AlertDialogCustom.showMensaggeDialog(this, "Agregar",
                        getString(R.string.exito_trabajo), (click, pos) -> closeActivity());

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("DATA", res);
        Toast.makeText(getApplicationContext(), "Se agrego correctamente", Toast.LENGTH_SHORT).show();

    }

    private void closeActivity(){
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putBoolean("registro", true);
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }


    private void limpiar(){
        txtTrabajo.setText("");
        txtEmail.setText("");
        txtTelefono.setText("");
        txtEmpresa.setText("");
        //txtTitulo.setText("");
        txtDescripcion.setText("");
        txtDireccion.setText("");
        txtWeb.setText("");
        txtTipo.setText("");
        txtSueldo.setText("");
        isEdit = false;
        ID = 0;
    }


    private boolean validar(){
        if(validarCampo(txtTrabajo) || validarCampo(txtTelefono) ||
                validarCampo(txtDescripcion)  || validarCampo(txtDireccion))
            return false;
        else
            return true;
    }
    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }


    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

}
