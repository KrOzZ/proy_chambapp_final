package com.tecktools.proy_chambapp.activitys;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.tecktools.proy_chambapp.R;
import com.tecktools.proy_chambapp.adapter.TrabajoAdapter;
import com.tecktools.proy_chambapp.clases.Login;
import com.tecktools.proy_chambapp.clases.Trabajos;
import com.tecktools.proy_chambapp.database.BaseURL;
import com.tecktools.proy_chambapp.database.TrabajoPHP;
import com.tecktools.proy_chambapp.utils.LoaderList;
import com.tecktools.proy_chambapp.utils.ViewAnimation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListTrabajoActivity extends AppCompatActivity {
    private TextView lblUser;

    private Login login;
    RecyclerView listViews;
    CardView cardView;
    private View back_drop;
    private SearchView srcLista;
    boolean isSearchBarHide = false;
    private ArrayList<String> arrayList;
    private TrabajoAdapter adapter;
    private View lytAddTrabajo;
    private FloatingActionButton fabAddTrabajo;
    private View lytTipo;
    private FloatingActionButton fabTipo;
    private FloatingActionButton mFabAdd;
    private View mLytMic1;
    private Toolbar toolbar;

    private BottomNavigationView navigation;
    private boolean rotate = false;
    private ActionBar actionBar;


    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private Button btnNuevo;
    private RecyclerView listTrabajos;
    private TrabajoPHP ws;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Trabajos> list;
    RequestQueue queue;
    private Trabajos traba;
    private View bottom_sheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_trabajo);
        //getSupportActionBar().setTitle("Trabajos");
        request = Volley.newRequestQueue(this);
        initToolbar();
        initNavigationMenu();
        lblUser = (TextView) findViewById(R.id.lblUser);
        navigation = findViewById(R.id.navigation);
        Bundle datos = getIntent().getExtras();
        lblUser.setText("Nombre: " + datos.getString("user"));
        this.login = (Login) getIntent().getExtras().getSerializable("login");
        init();


        //listViews = (ListView) findViewById(R.id.lvTrabajos);
        //cardView = (CardView) findViewById(R.id.cardView);
        //listViews.setOnScrollChangeListener(this::actionScroll);


    }


    private void initNavigationMenu() {
        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(this::onItemClickMenu);
    }

    private boolean onItemClickMenu(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuItemPerfil:
                Toast.makeText(this, "Perfil",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.menuItemDatos:
                Intent intent = new Intent(this, InformationActivity.class);
                startActivity(intent);
                break;
            case R.id.menuItemConfig:
                Toast.makeText(this, "config",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.menuItemCloset:
                finish();
                break;

        }
        return true;
    }


    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Chambas");

    }

    private void actionScroll(View view, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (scrollY < oldScrollY) { // up

            animateNavigation(true);

            //animateSearchBar(false);
        }
        if (scrollY > oldScrollY) { // down
            animateNavigation(false);
            //animateSearchBar(true);
        }
    }


    private void animateSearchBar(final boolean hide) {
        if (isSearchBarHide && hide || !isSearchBarHide && !hide) return;
        isSearchBarHide = hide;
        int moveY = hide ? -(2 * toolbar.getHeight()) : 0;
        toolbar.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
    }

    boolean isNavigationHide = false;

    private void animateNavigation(final boolean hide) {
        if (isNavigationHide && hide || !isNavigationHide && !hide) return;
        isNavigationHide = hide;
        int moveY = hide ? (2 * navigation.getHeight()) : 0;
        navigation.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
    }

    private void init() {
        lytAddTrabajo = findViewById(R.id.lyt_AddTrabajo);
        lytTipo = findViewById(R.id.lyt_Tipo);

        fabAddTrabajo = findViewById(R.id.fab_AddTrabajo);
        fabTipo = findViewById(R.id.fab_Tipos);

        mFabAdd = findViewById(R.id.fab_add);
        back_drop = findViewById(R.id.back_drop);

        ViewAnimation.initShowOut(this.lytTipo);
        ViewAnimation.initShowOut(this.lytAddTrabajo);
        back_drop.setVisibility(View.GONE);

        mFabAdd.setOnClickListener(this::toggleFabMode);
        back_drop.setOnClickListener(v -> toggleFabMode(mFabAdd));
        fabTipo.setOnClickListener(v -> Toast.makeText(this, "Tipos de empleos",
                Toast.LENGTH_SHORT).show());
        fabAddTrabajo.setOnClickListener(this::openTrabajo);

        queue = Volley.newRequestQueue(this);
        listTrabajos = findViewById(R.id.lvTrabajos);
        listTrabajos.setLayoutManager(new LinearLayoutManager(this));
        listTrabajos.setHasFixedSize(true);
        consultTrabajos();


        ws = new TrabajoPHP(getApplicationContext());
        list = new ArrayList<>();
    }

    private void consultTrabajos() {
        //LoaderList.showLoading(findViewById(R.id.lyt_progress), listTrabajos, findViewById(R.id.lyt_empty));
        StringRequest request = new StringRequest(Request.Method.POST,
                BaseURL.URL_CONSULTAR_TODOS,
                this::loadTrabajos,
                error ->{
                    LoaderList.showEmpty(findViewById(R.id.lyt_progress), findViewById(R.id.lyt_empty));
                    Log.e("MSG", error.getMessage());

                });
        queue.add(request);

    }
    private void consultTrabajosMios() {
        //LoaderList.showLoading(findViewById(R.id.lyt_progress), listTrabajos, findViewById(R.id.lyt_empty));
        StringRequest request = new StringRequest(Request.Method.POST,
                BaseURL.URL_CONSULTAR_TODOS,
                this::loadTrabajos,
                error ->{
                    LoaderList.showEmpty(findViewById(R.id.lyt_progress), findViewById(R.id.lyt_empty));
                    Log.e("MSG", error.getMessage());

                });
        queue.add(request);

    }

    private void loadTrabajos(String response) {
        Log.e("MSG", response);
        try {
            ArrayList<Trabajos> list = new ArrayList<>();
            JSONObject object = new JSONObject(response);
            JSONArray array = object.getJSONArray("trabajos");
            if (array.length() == 0) {
                LoaderList.showEmpty(findViewById(R.id.lyt_progress), findViewById(R.id.lyt_empty));
            } else {
                for (int x = 0; x < array.length(); x++) {
                    list.add(new Trabajos(array.getJSONObject(x)));
                }
            }
            adapter = new TrabajoAdapter(list, this);
            adapter.setOnItemClickListener(this::actionItemTrabajo);
            listTrabajos.setAdapter(adapter);
            LoaderList.showList(findViewById(R.id.lyt_progress), listTrabajos);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("MSG", response);
    }

    private void actionItemTrabajo(View view, Trabajos trabajos, int i) {
        Log.e("MDG", String.valueOf(i));
        showCustomDialog(trabajos);

    }
    private void showCustomDialog(Trabajos trabajo) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_event);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final TextView txtTrabajo= dialog.findViewById(R.id.txtTrabajo);
        final TextView txtEmail= dialog.findViewById(R.id.txtEmail);
        final TextView txtTelefono= dialog.findViewById(R.id.txtTelefono);
        final TextView txtSueldo= dialog.findViewById(R.id.txtSueldo);
        final TextView txtDescrop= dialog.findViewById(R.id.txtDescrip);
        final TextView txtDireccion= dialog.findViewById(R.id.txtDireccion);
        final TextView txtEmpresa= dialog.findViewById(R.id.txtEmpresa);
        final TextView txtTipo= dialog.findViewById(R.id.txtTipo);



        txtTrabajo.setText(trabajo.getTrabajo());
        txtEmail.setText(trabajo.getEmail());
        txtTelefono.setText(trabajo.getTelefono());
        txtSueldo.setText(String.valueOf( trabajo.getSueldo()));
        switch (trabajo.getTipo()){
            case 1:
                txtTipo.setText("Informatica");
                break;
            case 2:
                txtTipo.setText("Hoteleria");
                break;
            case 3:
                txtTipo.setText("Comercio");
                break;
            case 4:
                txtTipo.setText("Restaurante");
                break;
            case 5:
                txtTipo.setText("Medicina");
                break;
            case 6:
                txtTipo.setText("Ingeneria");
                break;
            case 7:
                txtTipo.setText("Servicios");
                break;
        }

        txtDescrop.setText(trabajo.getDescripcion());
        txtDireccion.setText(trabajo.getDireccion());
        txtEmpresa.setText(trabajo.getEmpresa());


        //String[] timezones = getResources().getStringArray(R.array.timezone);


        ((ImageButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        /*((Button) dialog.findViewById(R.id.bt_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*/

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data == null){
            return;
        }

        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                consultTrabajos();
            }
        }

    }



    private void openTrabajo(View view) {
        Intent intent = new Intent(this, RegisTrabajoActivity.class);
        startActivityForResult(intent, 1);
    }

    private void toggleFabMode(View paramView) {
        this.rotate = ViewAnimation.rotateFab(paramView, !this.rotate);
        if (this.rotate) {
            ViewAnimation.showIn(this.lytTipo);
            ViewAnimation.showIn(this.lytAddTrabajo);
            this.back_drop.setVisibility(View.VISIBLE);
        } else {
            ViewAnimation.showOut(this.lytTipo);
            ViewAnimation.showOut(this.lytAddTrabajo);
            this.back_drop.setVisibility(View.GONE);
        }
    }


}