package com.tecktools.proy_chambapp.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.tecktools.proy_chambapp.MainActivity;
import com.tecktools.proy_chambapp.R;
import com.tecktools.proy_chambapp.clases.Login;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private ProgressBar progress_bar;
    private FloatingActionButton fab;
    private View parent_view;
    private LinearLayout signUpForAccount;
    private TextView btnRegistro;
    private TextView btnOlvido;
    private EditText txtPassword;
    private EditText txtUser;
    private Login login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        parent_view = findViewById(android.R.id.content);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        txtPassword= findViewById(R.id.txtPassword);
        txtUser= findViewById(R.id.txtUser);

        signUpForAccount = findViewById(R.id.sign_up_for_account);

        btnRegistro = findViewById(R.id.btnRegistro);
        btnOlvido = findViewById(R.id.btnOlvido);
        login = new Login();



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                Toast.makeText(LoginActivity.this,"Bienvenido "+ txtUser.getText().toString(),Toast.LENGTH_LONG).show();
                validarUsuario("https://vivamazatlantours.com/Practicas/ChambApp_DB/validar_usuario.php");
            }
                /*String user = txtUser.getText().toString();
                String pass = txtPassword.getText().toString();

                login.setUser(user);
                login.setPass(pass);

                if (login.Validar())
                {
                    Intent intent = new Intent(LoginActivity.this,ListTrabajoActivity.class);
                    intent.putExtra("user", user);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("login", login );
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                else
                    searchAction();

            }*/
        });

        TextView btnRegistro = (TextView) findViewById(R.id.btnRegistro);
        TextView btnOlvido = (TextView) findViewById(R.id.btnOlvido);

        btnRegistro.setMovementMethod(LinkMovementMethod.getInstance());
        btnOlvido.setMovementMethod(LinkMovementMethod.getInstance());

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(parent_view, "HOLA", Snackbar.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivity.this, RegisCuentaActivity.class);

                startActivity(intent);
            }
        });
        btnOlvido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(parent_view,"Olvidaste",Snackbar.LENGTH_SHORT).show();
                //Intent intent = new Intent(LoginActivity.this,PasswordActivity.class);
                //startActivity(intent);
            }
        });



    }
    private void validarUsuario(String URL) {
        String user = txtUser.getText().toString();
        String pass = txtPassword.getText().toString();

        login.setUser(user);
        login.setPass(pass);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (!response.isEmpty()) {
                    Intent intent = new Intent(LoginActivity.this, ListTrabajoActivity.class);
                    intent.putExtra("user", user);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("login", login);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    searchAction();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("MSG", error.getMessage());
                //Toast.makeText(LoginActivity.this ,error.toString(),Toast.LENGTH_SHORT).show();
            }

        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parametros= new HashMap<String, String>();
                parametros.put("usuario",txtUser.getText().toString());
                parametros.put("password",txtPassword.getText().toString());
                return parametros;
            }
        };
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    //Para Las Pruevas
    private void searchAction() {

        progress_bar.setVisibility(View.VISIBLE);
        fab.setAlpha(0f);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progress_bar.setVisibility(View.GONE);
                fab.setAlpha(1f);
                Snackbar.make(parent_view, "Usuario o Contraseña Incorrectra", Snackbar.LENGTH_SHORT).show();
            }
        }, 1000);
    }
}
